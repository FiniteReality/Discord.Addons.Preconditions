﻿using Discord.Addons.Preconditions.Validation;
using Discord.Commands;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class MinLengthTests : IClassFixture<ParameterPreconditionTestFixture>
    {
        private readonly ParameterPreconditionTestFixture fixture;
        private readonly MinLengthAttribute test = new MinLengthAttribute(5);

        private ParameterInfo LongParameter => fixture.LongParameter;
        private ParameterInfo StringParameter => fixture.StringParameter;
        private IServiceProvider Services => fixture.Services;
        private ICommandContext Context => fixture.Context;

        public MinLengthTests(ParameterPreconditionTestFixture fixture) => this.fixture = fixture;

        [Fact]
        public async Task Empty()
        {
            var empty = await test.CheckPermissions(Context, StringParameter, "", Services);
            Assert.Equal(false, empty.IsSuccess);
        }
        [Fact]
        public async Task Below()
        {
            var below = await test.CheckPermissions(Context, StringParameter, "bad", Services);
            Assert.Equal(false, below.IsSuccess);
        }
        [Fact]
        public async Task Equal()
        {
            var equal = await test.CheckPermissions(Context, StringParameter, "quint", Services);
            Assert.Equal(true, equal.IsSuccess);
        }
        [Fact]
        public async Task Above()
        {
            var above = await test.CheckPermissions(Context, StringParameter, "bamboozled", Services);
            Assert.Equal(true, above.IsSuccess);
        }
        [Fact]
        public async Task Unicode()
        {
            var unicode = await test.CheckPermissions(Context, StringParameter, "quince\u180E", Services);
            Assert.Equal(true, unicode.IsSuccess);
        }
        [Fact]
        public async Task WithInt()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await test.CheckPermissions(Context, LongParameter, "appel", Services));
        }

    }
}
