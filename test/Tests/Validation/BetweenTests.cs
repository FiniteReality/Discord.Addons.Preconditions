﻿using Discord.Addons.Preconditions.Validation;
using Discord.Commands;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests.Validation
{
    public class BetweenTests : IClassFixture<ParameterPreconditionTestFixture>
    {
        private readonly ParameterPreconditionTestFixture fixture;
        private readonly BetweenAttribute test = new BetweenAttribute(10, 20);

        private ParameterInfo LongParameter => fixture.LongParameter;
        private ParameterInfo StringParameter => fixture.StringParameter;
        private IServiceProvider Services => fixture.Services;
        private ICommandContext Context => fixture.Context;

        public BetweenTests(ParameterPreconditionTestFixture fixture) => this.fixture = fixture;

        [Fact]
        public async Task BelowMinimum()
        {
            var below = await test.CheckPermissions(Context, LongParameter, 1, Services);
            Assert.Equal(false, below.IsSuccess);
        }
        [Fact]
        public async Task EqualMinimum()
        {
            var equal = await test.CheckPermissions(Context, LongParameter, 10, Services);
            Assert.Equal(true, equal.IsSuccess);
        }
        [Fact]
        public async Task Between()
        {
            var above = await test.CheckPermissions(Context, LongParameter, 15, Services);
            Assert.Equal(true, above.IsSuccess);
        }
        [Fact]
        public async Task EqualMaximum()
        {
            var below = await test.CheckPermissions(Context, LongParameter, 20, Services);
            Assert.Equal(true, below.IsSuccess);
        }
        [Fact]
        public async Task AboveMaximum()
        {
            var equal = await test.CheckPermissions(Context, LongParameter, 2000, Services);
            Assert.Equal(false, equal.IsSuccess);
        }
        [Fact]
        public async Task WithString()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await test.CheckPermissions(Context, StringParameter, 0, Services));
        }
    }
}
