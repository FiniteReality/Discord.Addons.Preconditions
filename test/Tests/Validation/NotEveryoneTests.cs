﻿using Discord;
using Discord.Addons.Preconditions.Validation;
using Discord.Commands;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests.Validation
{
    public class NotEveryoneTests : IClassFixture<ParameterPreconditionTestFixture>
    {
        private readonly ParameterPreconditionTestFixture fixture;
        private readonly NotEveryoneAttribute test = new NotEveryoneAttribute();
        private readonly Mock<IGuild> _guildMock;

        private ParameterInfo StringParameter => fixture.StringParameter;
        private ParameterInfo RoleParameter => fixture.RoleParameter;
        private IServiceProvider Services => fixture.Services;
        private ICommandContext Context => fixture.Context;

        public NotEveryoneTests(ParameterPreconditionTestFixture fixture)
        {
            _guildMock = new Mock<IGuild>();

            _guildMock.Setup(x => x.EveryoneRole.Id).Returns(1234);
            _guildMock.Setup(x => x.EveryoneRole.Guild).Returns(() => _guildMock.Object);

            this.fixture = fixture;
        }

        [Fact]
        public async Task Everyone()
        {
            var guild = _guildMock.Object;
            var everyone = await test.CheckPermissions(Context, RoleParameter, guild.EveryoneRole, Services);
            Assert.Equal(false, everyone.IsSuccess);
        }

        [Fact]
        public async Task NotEveryone()
        {
            var roleMock = new Mock<IRole>();
            roleMock.Setup(x => x.Id).Returns(4321);
            roleMock.Setup(x => x.Guild).Returns(() => _guildMock.Object);

            var role = roleMock.Object;
            var everyone = await test.CheckPermissions(Context, RoleParameter, role, Services);
            Assert.Equal(true, everyone.IsSuccess);
        }

        [Fact]
        public async Task WithoutRole()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await test.CheckPermissions(Context, StringParameter, "hello world", Services));
        }
    }
}
