﻿using Discord.Addons.Preconditions.Validation;
using Discord.Commands;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class AtMostTests : IClassFixture<ParameterPreconditionTestFixture>
    {
        private readonly ParameterPreconditionTestFixture fixture;
        private readonly AtMostAttribute test = new AtMostAttribute(2000);

        private ParameterInfo LongParameter => fixture.LongParameter;
        private ParameterInfo StringParameter => fixture.StringParameter;
        private IServiceProvider Services => fixture.Services;
        private ICommandContext Context => fixture.Context;

        public AtMostTests(ParameterPreconditionTestFixture fixture) => this.fixture = fixture;

        [Fact]
        public async Task Below()
        {
            var below = await test.CheckPermissions(Context, LongParameter, 20, Services);
            Assert.Equal(true, below.IsSuccess);
        }
        [Fact]
        public async Task Equal()
        {
            var equal = await test.CheckPermissions(Context, LongParameter, 2000, Services);
            Assert.Equal(true, equal.IsSuccess);
        }
        [Fact]
        public async Task Above()
        {
            var above = await test.CheckPermissions(Context, LongParameter, 3000, Services);
            Assert.Equal(false, above.IsSuccess);
        }
        [Fact]
        public async Task WithString()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await test.CheckPermissions(Context, StringParameter, 0, Services));
        }
    }
}
