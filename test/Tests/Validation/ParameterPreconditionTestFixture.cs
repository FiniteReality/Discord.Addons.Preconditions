﻿using Discord;
using Discord.Commands;
using System;
using System.Threading.Tasks;

namespace Tests
{
    public class ParameterPreconditionTestFixture
    {
        public ParameterInfo LongParameter { get; private set; }
        public ParameterInfo StringParameter { get; private set; }
        public ParameterInfo RoleParameter { get; private set; }
        public ParameterInfo GuildUserParameter { get; private set; }
        public IServiceProvider Services => MockServiceProvider.Instance;
        public ICommandContext Context => new MockCommandContext();

        public ParameterPreconditionTestFixture()
        {
            var service = new CommandService();
            var module = service.CreateModuleAsync("x", x =>
            {
                x.AddCommand("x", (ctx, p, s, c) => Task.CompletedTask, b =>
                {
                    b.AddParameter("integer", typeof(int), c => { });
                    b.AddParameter("string", typeof(string), c => { });
                    b.AddParameter("role", typeof(IRole), c => { });
                    b.AddParameter("guildUser", typeof(IGuildUser), c => { });
                });
            }).Result;
            LongParameter = module.Commands[0].Parameters[0];
            StringParameter = module.Commands[0].Parameters[1];
            RoleParameter = module.Commands[0].Parameters[2];
            GuildUserParameter = module.Commands[0].Parameters[3];
        }
    }

    internal sealed class MockCommandContext : ICommandContext
    {
        public IDiscordClient Client => throw new NotImplementedException();
        public IGuild Guild => throw new NotImplementedException();
        public IMessageChannel Channel => throw new NotImplementedException();
        public IUser User => throw new NotImplementedException();
        public IUserMessage Message => throw new NotImplementedException();
    }
}
