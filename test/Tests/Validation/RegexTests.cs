﻿using Discord.Addons.Preconditions.Validation;
using Discord.Commands;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class RegexTests : IClassFixture<ParameterPreconditionTestFixture>
    {
        private readonly ParameterPreconditionTestFixture fixture;
        private readonly RegexAttribute testMatches = new RegexAttribute(@"!+\w+", matchMode: RegexMatchMode.RequireMatch);
        private readonly RegexAttribute testNoMatch = new RegexAttribute(@"!+\w+", matchMode: RegexMatchMode.RequireNoMatch);

        private ParameterInfo LongParameter => fixture.LongParameter;
        private ParameterInfo StringParameter => fixture.StringParameter;
        private IServiceProvider Services => fixture.Services;
        private ICommandContext Context => fixture.Context;

        public RegexTests(ParameterPreconditionTestFixture fixture) => this.fixture = fixture;

        [Fact]
        public async Task Matches_With_Match()
        {
            var test = await testMatches.CheckPermissions(Context, StringParameter, "!!!yes", Services);
            Assert.Equal(true, test.IsSuccess);
        }
        [Fact]
        public async Task Matches_Without_Match()
        {
            var test = await testMatches.CheckPermissions(Context, StringParameter, "???no", Services);
            Assert.Equal(false, test.IsSuccess);
        }
        [Fact]
        public async Task NoMatch_With_Match()
        {
            var test = await testNoMatch.CheckPermissions(Context, StringParameter, "!!!yes", Services);
            Assert.Equal(false, test.IsSuccess);
        }
        [Fact]
        public async Task NoMatch_Without_Match()
        {
            var test = await testNoMatch.CheckPermissions(Context, StringParameter, "???no", Services);
            Assert.Equal(true, test.IsSuccess);
        }
        [Fact]
        public async Task Matches_WithInt()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await testMatches.CheckPermissions(Context, LongParameter, "appel", Services));
        }
        [Fact]
        public async Task NoMatch_WithInt()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await testNoMatch.CheckPermissions(Context, LongParameter, "appel", Services));
        }
    }
}
