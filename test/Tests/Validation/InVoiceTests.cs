using Discord;
using Discord.Addons.Preconditions.Validation;
using Discord.Commands;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class InVoiceTests : IClassFixture<ParameterPreconditionTestFixture>
    {
        private readonly ParameterPreconditionTestFixture fixture;
        private readonly InVoiceAttribute _testNoFilter = new InVoiceAttribute();
        private readonly InVoiceAttribute _testFilter = new InVoiceAttribute(1234);
        private readonly Mock<IGuildUser> _userMock;
        private readonly Mock<IVoiceChannel> _channelMock;

        private ParameterInfo GuildUserParameter => fixture.GuildUserParameter;
        private ParameterInfo StringParameter => fixture.StringParameter;
        private IServiceProvider Services => fixture.Services;
        private ICommandContext Context => fixture.Context;

        public InVoiceTests(ParameterPreconditionTestFixture fixture)
        {
            this.fixture = fixture;

            _userMock = new Mock<IGuildUser>();
            _channelMock = new Mock<IVoiceChannel>();

            _channelMock.Setup(x => x.Name).Returns("Test channel");
            _channelMock.Setup(x => x.Id).Returns(1234);

            _userMock.Setup(x => x.Guild.GetVoiceChannelAsync(
                It.IsAny<ulong>(), It.IsAny<CacheMode>(), It.IsAny<RequestOptions>()
            )).ReturnsAsync(() => _channelMock.Object);
        }

        [Fact]
        public async Task NotInVoiceUnfiltered()
        {
            _userMock.Setup(x => x.VoiceChannel).Returns(() => null);

            var user = _userMock.Object;
            var inVoice = await _testNoFilter.CheckPermissions(Context, GuildUserParameter, user, Services);
            Assert.Equal(false, inVoice.IsSuccess);
        }

        [Fact]
        public async Task InVoiceUnfiltered()
        {
            _userMock.Setup(x => x.VoiceChannel).Returns(() => _channelMock.Object);

            var user = _userMock.Object;
            var inVoice = await _testNoFilter.CheckPermissions(Context, GuildUserParameter, user, Services);
            Assert.Equal(true, inVoice.IsSuccess);
        }

        [Fact]
        public async Task NotInVoiceFiltered()
        {
            _userMock.Setup(x => x.VoiceChannel).Returns(() => null);

            var user = _userMock.Object;
            var inVoice = await _testFilter.CheckPermissions(Context, GuildUserParameter, user, Services);
            Assert.Equal(false, inVoice.IsSuccess);
        }

        [Fact]
        public async Task InVoiceFiltered()
        {
            _userMock.Setup(x => x.VoiceChannel).Returns(() => _channelMock.Object);

            var user = _userMock.Object;
            var inVoice = await _testFilter.CheckPermissions(Context, GuildUserParameter, user, Services);
            Assert.Equal(true, inVoice.IsSuccess);
        }

        [Fact]
        public async Task String()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await _testNoFilter.CheckPermissions(Context, StringParameter, "Hello", Services));
        }
    }
}
