using Discord;
using Discord.Addons.Preconditions.Validation;
using Discord.Commands;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class InRoleTests : IClassFixture<ParameterPreconditionTestFixture>
    {
        private readonly ParameterPreconditionTestFixture fixture;
        private readonly InRoleAttribute test = new InRoleAttribute(1234);
        private readonly Mock<IGuildUser> _userMock;

        private ParameterInfo GuildUserParameter => fixture.GuildUserParameter;
        private ParameterInfo StringParameter => fixture.StringParameter;
        private IServiceProvider Services => fixture.Services;
        private ICommandContext Context => fixture.Context;

        public InRoleTests(ParameterPreconditionTestFixture fixture)
        {
            this.fixture = fixture;

            _userMock = new Mock<IGuildUser>();

            _userMock.Setup(x => x.Guild.GetRole(1234).Name).Returns("test role");
        }

        [Fact]
        public async Task NotInRole()
        {
            _userMock.Setup(x => x.RoleIds).Returns(new ulong[]{});

            var user = _userMock.Object;
            var inRole = await test.CheckPermissions(Context, GuildUserParameter, user, Services);
            Assert.Equal(false, inRole.IsSuccess);
        }

        [Fact]
        public async Task InRole()
        {
            _userMock.Setup(x => x.RoleIds).Returns(new ulong[]{1234});

            var user = _userMock.Object;
            var inRole = await test.CheckPermissions(Context, GuildUserParameter, user, Services);
            Assert.Equal(true, inRole.IsSuccess);
        }

        [Fact]
        public async Task String()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await test.CheckPermissions(Context, StringParameter, "Hello", Services));
        }
    }
}
