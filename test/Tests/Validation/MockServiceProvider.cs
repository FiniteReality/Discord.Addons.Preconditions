﻿using System;

namespace Tests
{
    internal class MockServiceProvider : IServiceProvider
    {
        public static MockServiceProvider Instance => new MockServiceProvider();

        private MockServiceProvider()
        { }

        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }
    }
}