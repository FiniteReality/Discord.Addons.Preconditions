﻿using Discord.Commands;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Discord.Addons.Preconditions.Validation
{
    /// <summary>
    /// A parameter precondition which succeeds when the value passed
    /// matches <see cref="Regex"/>.
    /// </summary>
    /// <example>
    /// [Command("roll")]
    /// public async Task RollDiceAsync([Regex("\d+d\d+")]string dice)
    /// {
    ///     await ReplyAsync($"Rolling a {dice}..."); //e.g. Rolling a 2d6
    /// }
    /// </example>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public class RegexAttribute : ParameterPreconditionAttribute
    {
        /// <summary>
        /// The regular expression that this precondition has to match.
        /// </summary>
        public Regex Regex { get; }
        /// <summary>
        /// The match mode that this precondition follows.
        /// </summary>
        public RegexMatchMode MatchMode { get; }

        /// <summary>
        /// Creates a new instance of <see cref="RegexAttribute"/>
        /// </summary>
        /// <param name="regex">The regular expression that will be matched</param>
        /// <param name="options">The options passed to the regular expression</param>
        /// <param name="matchMode">The match mode to follow</param>
        public RegexAttribute(string regex, RegexOptions options = RegexOptions.None, RegexMatchMode matchMode = RegexMatchMode.RequireMatch)
        {
            Regex = new Regex(regex, options);
            MatchMode = matchMode;
        }


        /// <inheritdoc/>
        public override Task<PreconditionResult> CheckPermissions(ICommandContext context, ParameterInfo parameter, object value, IServiceProvider services)
        {
            Preconditions.IsString(parameter.Type, nameof(RegexAttribute), nameof(value));

            bool matches = Regex.IsMatch((string)value);

            if (MatchMode == RegexMatchMode.RequireMatch)
            {
                if (matches)
                    return Task.FromResult(PreconditionResult.FromSuccess());
                else
                    return Task.FromResult(PreconditionResult.FromError($"The parameter {parameter.Name} must match the regular expression {Regex}"));
            }
            else
            {
                if (!matches)
                    return Task.FromResult(PreconditionResult.FromSuccess());
                else
                    return Task.FromResult(PreconditionResult.FromError($"The parameter {parameter.Name} must not match the regular expression {Regex}"));
            }
        }
    }

    /// <summary>
    /// Match modes for <see cref="RegexAttribute"/>
    /// </summary>
    public enum RegexMatchMode
    {
        /// <summary>
        /// Require that the value matches the regex
        /// </summary>
        RequireMatch,
        /// <summary>
        /// Require that the value does not match the regex
        /// </summary>
        RequireNoMatch
    }
}
