﻿using System;
using System.Threading.Tasks;
using Discord.Commands;

namespace Discord.Addons.Preconditions.Validation
{
    /// <summary>
    /// A parameter precondition which succeeds when the value passed
    /// is less than or equal to <see cref="MaxLength"/> characters.
    /// </summary>
    /// <example>
    /// [Command("reminder")]
    /// public async Task RemindAsync(int minutes, [Remainder, MaxLenth(100)]string message)
    /// {
    ///     await ReplyAsync("Ok! I'll remind you in {minutes} minute(s).");
    /// }
    /// </example>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public class MaxLengthAttribute : ParameterPreconditionAttribute
    {
        /// <summary>
        /// The maximum length that this precondition will accept, inclusively.
        /// </summary>
        public int MaxLength { get; }

        /// <summary>
        /// Creates a new instance of <see cref="MaxLengthAttribute"/>
        /// </summary>
        /// <param name="length">The maximum length that this precondition will accept, inclusively.</param>
        public MaxLengthAttribute(int length)
        {
            MaxLength = length;
        }

        /// <inheritdoc/>
        public override Task<PreconditionResult> CheckPermissions(ICommandContext context, ParameterInfo parameter, object value, IServiceProvider services)
        {
            Preconditions.IsString(parameter.Type, nameof(MaxLengthAttribute), nameof(value));

            if (((string)value).Length <= MaxLength)
                return Task.FromResult(PreconditionResult.FromSuccess());
            else
                return Task.FromResult(PreconditionResult.FromError($"Parameter {parameter.Name} must be at most {MaxLength} characters."));
        }
    }
}
