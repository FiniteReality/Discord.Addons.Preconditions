﻿using Discord.Commands;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Discord.Addons.Preconditions.Validation
{
    /// <summary>
    /// A parameter precondition which succeeds when the <see cref="IGuildUser"/> is
    /// in the <see cref="RoleId"/> role.
    /// </summary>
    public class InRoleAttribute : ParameterPreconditionAttribute
    {
        private static readonly Type _IGuildUserType = typeof(IGuildUser);

        /// <summary>
        /// The Id for the <see cref="IRole"/> which is required
        /// </summary>
        public ulong RoleId { get; }

        /// <summary>
        /// Creates a new instance of <see cref="InRoleAttribute"/>
        /// </summary>
        /// <param name="roleId">The Id for the <see cref="IRole"/> which is required</param>
        public InRoleAttribute(ulong roleId)
        {
            RoleId = roleId;
        }

        /// <inheritdoc/>
        public override Task<PreconditionResult> CheckPermissions(ICommandContext context, ParameterInfo parameter, object value, IServiceProvider services)
        {
            Preconditions.Inherits(_IGuildUserType, parameter.Type, nameof(value));

            var user = (IGuildUser)value;
            var guild = user.Guild;
            var role = guild.GetRole(RoleId);

            if (!user.RoleIds.Contains(RoleId))
                return Task.FromResult(PreconditionResult.FromError($"User must be in the {role.Name} role"));
            else
                return Task.FromResult(PreconditionResult.FromSuccess());
        }
    }
}
