﻿using Discord.Commands;
using System;
using System.Threading.Tasks;

namespace Discord.Addons.Preconditions.Validation
{
    /// <summary>
    /// A parameter precondition which succeeds when the value passed
    /// is greater than or equal to <see cref="MinLength"/> characters.
    /// </summary>
    /// <example>
    /// [Command("reminder")]
    /// public async Task RemindAsync(int minutes, [Remainder, MinLenth(20)]string message)
    /// {
    ///     await ReplyAsync("Ok! I'll remind you in {minutes} minute(s).");
    /// }
    /// </example>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public class MinLengthAttribute : ParameterPreconditionAttribute
    {
        /// <summary>
        /// The minimum length that this precondition will accept, inclusively.
        /// </summary>
        public int MinLength { get; }


        /// <summary>
        /// Creates a new instance of <see cref="MinLengthAttribute"/>
        /// </summary>
        /// <param name="length">The minimum length that this precondition will accept, inclusively.</param>
        public MinLengthAttribute(int length)
        {
            MinLength = length;
        }

        /// <inheritdoc/>
        public override Task<PreconditionResult> CheckPermissions(ICommandContext context, ParameterInfo parameter, object value, IServiceProvider services)
        {
            Preconditions.IsString(parameter.Type, nameof(MinLengthAttribute), nameof(value));

            if (((string)value).Length >= MinLength)
                return Task.FromResult(PreconditionResult.FromSuccess());
            else
                return Task.FromResult(PreconditionResult.FromError($"Parameter {parameter.Name} must be at least {MinLength} characters."));
        }
    }
}
