﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Discord.Addons.Preconditions.Validation
{
    /// <summary>
    /// A parameter precondition which succeeds when the <see cref="IRole"/> passed
    /// is not the guild's everyone role
    /// </summary>
    /// <example>
    /// [Command("mention")]
    /// public async Task MentionAsync([NotEveryone]SocketRole role, [Remainder]string message)
    /// {
    ///     await ReplyAsync($"Hey {role.Mention}! {message}");
    /// }
    /// </example>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public class NotEveryoneAttribute : ParameterPreconditionAttribute
    {
        private static readonly Type _IRoleType = typeof(IRole);

        /// <inheritdoc/>
        public override Task<PreconditionResult> CheckPermissions(ICommandContext context, ParameterInfo parameter, object value, IServiceProvider services)
        {
            Preconditions.Inherits(_IRoleType, parameter.Type, nameof(value));

            IRole role = (IRole)value;
            if (role.Id == role.Guild.EveryoneRole.Id)
                return Task.FromResult(PreconditionResult.FromError("The role must not be @\u200beveryone or @\u200bhere."));
            else
                return Task.FromResult(PreconditionResult.FromSuccess());
        }
    }
}
