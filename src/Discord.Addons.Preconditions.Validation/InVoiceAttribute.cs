using Discord.Commands;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Discord.Addons.Preconditions.Validation
{
    /// <summary>
    /// A parameter precondition which succeeds when the
    /// <see cref="IGuildUser"/> is in a voice channel, optionally specifying
    /// the <see cref="ChannelId"/> to check.
    /// </summary>
    public class InVoiceAttribute : ParameterPreconditionAttribute
    {
        private static readonly Type _IGuildUserType = typeof(IGuildUser);

        /// <summary>
        /// The optional channel to check that the user is in
        /// </summary>
        public ulong? ChannelId { get; }

        /// <summary>
        /// Creates a new instance of <see cref="InVoiceAttribute"/> filtered
        /// to the given channel
        /// </summary>
        /// <param name="channelId">The channel id that is required</param>
        public InVoiceAttribute(ulong channelId)
        {
            ChannelId = channelId;
        }

        /// <summary>
        /// Creates a new instance of <see cref="InVoiceAttribute"/>
        /// </summary>
        public InVoiceAttribute()
        {
        }

        /// <inheritdoc/>
        public override async Task<PreconditionResult> CheckPermissions(ICommandContext context, ParameterInfo parameter, object value, IServiceProvider services)
        {
            Preconditions.Inherits(_IGuildUserType, parameter.Type, nameof(value));

            var user = (IGuildUser)value;
            var guild = user.Guild;

            if (ChannelId.HasValue)
            {
                var channel = await guild.GetVoiceChannelAsync(ChannelId.Value);
                if (user.VoiceChannel == null ||
                    user.VoiceChannel.Id != ChannelId.Value)
                    return PreconditionResult.FromError($"User must be in the {channel.Name} voice channel");
                else
                    return PreconditionResult.FromSuccess();
            }
            else
            {
                if (user.VoiceChannel == null)
                    return PreconditionResult.FromError("User must be in a voice channel");
                else
                    return PreconditionResult.FromSuccess();
            }
        }
    }
}
