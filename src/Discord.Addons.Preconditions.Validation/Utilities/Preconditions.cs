﻿using System;
using System.Reflection;

namespace Discord.Addons.Preconditions.Validation
{
    internal static class Preconditions
    {
        public static void AllowedNumericType(Type type, string typeName, string paramName)
        {
            if (!NumericTypes.IsSupportedNumericType(type))
                throw new ArgumentException($"{typeName} only supports one of {string.Join(", ", NumericTypes.Comparators.Keys)}", paramName);
        }

        public static void IsString(Type type, string typeName, string paramName)
        {
            if (type != typeof(string))
                throw new ArgumentException($"{typeName} only supports strings", paramName);
        }

        public static void Inherits(TypeInfo parent, TypeInfo type, string paramName)
        {
            if (!parent.IsAssignableFrom(type))
                throw new ArgumentException($"Only types inheriting from {parent.Name} are supported", paramName);
        }

        public static void Inherits(Type parent, Type type, string paramName)
            => Inherits(parent.GetTypeInfo(), type.GetTypeInfo(), paramName);
    }
}
