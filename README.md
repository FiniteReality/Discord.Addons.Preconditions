# Discord.Addons.Preconditions #
[![build status](https://gitlab.com/FiniteReality/Discord.Addons.Preconditions/badges/master/build.svg)](https://gitlab.com/FiniteReality/Discord.Addons.Preconditions/commits/master)

Useful preconditions for bot developers using Discord.Net 1.0

## Contributing ##

Check out the existing pull requests and issues if you want to help create a
new feature, or want to report an issue.

### Compiling ###

This section is mostly shamelessly copied from the Discord.Net README.
However, based on your workflow there are two main ways you can compile:

#### Visual Studio ####
- Visual Studio 2017 (with .NET Core workload)
- .NET Core SDK

#### Command line ####
- .NET Core SDK